#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import json
import os
import time

import paho.mqtt.client as mqtt
from vedirect import Vedirect

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='VE.Direct to MQTT bridge')
    parser.add_argument('--port', default=os.getenv('SERIAL', '/dev/ttyUSB0'), type=str, help='Serial port')
    parser.add_argument('--timeout', help='Serial port read timeout', type=int, default=os.getenv('SERIAL_TIMEOUT', 60))
    parser.add_argument('-H', '--mqtt-broker', help='mqtt broker address', type=str,
                        default=os.getenv('MQTT_BROKER', 'localhost'))
    parser.add_argument('-p', '--mqtt-port', help='mqtt broker port', type=int, default=os.getenv('MQTT_PORT', 1883))
    parser.add_argument('-t', '--topic', help='mqtt topic prefix', type=str,
                        default=os.environ.get('MQTT_TOPIC', 'vedirect'))
    args = parser.parse_args()
    ve = Vedirect(args.port, args.timeout)

    last = 0

    client = mqtt.Client()
    client.connect(args.mqtt_broker, args.mqtt_port, 60)
    client.loop_start()


    def mqtt_send_callback(packet):
        global last
        ignore = ["Checksum"]
        mapping = {
            "SER#": "serial",
            "PID": "type",
            "FW": "fw"
        }

        messages = [{}, {}]
        serial = ""
        for key, value in packet.items():
            if key in ignore:
                continue

            key = mapping.get(key, key)

            if key == "serial":
                serial = value

            if key in ["V", "I", "VPV", "IL"]:
                messages[0][key] = float(value) / 1000
            elif key in ["PPV", "CS", "MPPT", "ERR"]:
                messages[0][key] = int(value)
            elif key == "LOAD":
                messages[0][key] = int(value == 1)
            elif key in ["OR"]:
                messages[0][key] = int(value, 0)
            elif key in ["type", "fw", "serial"]:
                messages[1][key] = value
            else:
                messages[1][key] = int(value)

        if serial:
            client.publish(args.topic + '/' + serial, json.dumps(messages[0]))

            now = time.time()
            if now - last > 30:
                # send counters every 30 seconds
                client.publish(args.topic + '/' + serial, json.dumps(messages[1]))
                last = now


    ve.read_data_callback(mqtt_send_callback)
