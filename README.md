# VE.Direct to MQTT Exporter

Simple Victron VE.Direct to MQTT exporter

| env variable | description | 
| --- | --- |
| `SERIAL` | serial device (e.g. `/dev/ttyUSB0`) |
| `SERIAL_TIMEOUT` | serial timeout |
| `MQTT_BROKER` | hostname of MQTT broker |
| `MQTT_PORT` | port of MQTT broker |
| `MQTT_TOPIC` | MQTT base topic which is extended by the device's serial |

## Run Container

Run as docker container with MQTT broker on localhost (default setting): 
```
docker run registry.gitlab.com/nbuchwitz/vedirect-mqtt-exporter --name mppt_charger -e "SERIAL=/dev/ttyUSB0" -e "MQTT_TOPIC=pv"
```

## Example payload
The base topic in this example is `rv/vedirect`:
```
rv/vedirect/HQ19052UWZ1 2021-02-18T18:51:06+0100 {"V": 13.13, "I": 0.0, "VPV": 0.7, "PPV": 0, "CS": 0, "MPPT": 0, "OR": 1, "ERR": 0, "LOAD": 0}
rv/vedirect/HQ19052UWZ1 2021-02-18T18:51:06+0100 {"type": "0xA058", "fw": "154", "serial": "HQ19052UWZ1", "H19": 10888, "H20": 26, "H21": 54, "H22": 16, "H23": 66, "HSDS": 131}      
```

