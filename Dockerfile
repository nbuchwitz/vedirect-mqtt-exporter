FROM python:3.8 AS builder

COPY requirements.txt .

RUN pip install wheel
RUN pip install --user -r requirements.txt

FROM python:3.8-slim

WORKDIR /app

COPY --from=builder /root/.local /root/.local
COPY vedirect-mqtt.py .

ENV PATH=/root/.local/bin:$PATH

CMD [ "python", "./vedirect-mqtt.py" ]
